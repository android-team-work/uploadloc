package com.vineet.uploadloc;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.Random;

public class MyService extends Service {
    private DatabaseReference mDatabase;
    String location = "";
    Runnable runnable;
    LocationManager locationManager;
    Handler handler;

    public MyService() {
        handler = new Handler();
     /*   SharedPreferences sh = this.getSharedPreferences("samparkClientappref", MODE_PRIVATE);
        final SharedPreferences.Editor myEdit = sh.edit();



        userNameString  = sh.getString("useremail", "");
        userPasswordString = sh.getString("userpassword", "");*/

        //  str ="amit@gmail.com:Sanya@123";

        //
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        NotificationCompat.Builder notificationBuilder1 = new NotificationCompat.Builder(MyService.this, "dcccytuhp_92")
                .setContentTitle("h")
                .setContentText("h")
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                .setColor(ContextCompat.getColor(MyService.this, R.color.colorAccent))

                .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        long t = (new Date()).getTime();
        Random random = new Random();
        Log.e("Dek bhai dekh", "Date : " + t + " : " + (t + random.nextInt(99)));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {


            NotificationChannel channel12 = new NotificationChannel("dcccytuhp_92",
                    "Channel human hhh title",
                    NotificationManager.IMPORTANCE_HIGH);
            channel12.setLightColor(Color.BLUE);

            channel12.setShowBadge(true);

            notificationManager.createNotificationChannel(channel12);
        }
        startForeground((int) t + random.nextInt(99) + random.nextInt(999), notificationBuilder1.build());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        runnable = new Runnable() {
            @Override
            public void run() {
                Log.e("reeach","NAKA 0");
                if (ActivityCompat.checkSelfPermission(MyService.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MyService.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                Location nwLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (nwLocation != null) {
                    Log.e("reeach","NAKA 1");
                    double latitude = nwLocation.getLatitude();
                    double longitude = nwLocation.getLongitude();

                    location = ""+latitude+","+longitude;
                    Toast.makeText(
                            getApplicationContext(),
                            "Mobile Location (NW): \nLatitude: " + latitude
                                    + "\nLongitude: " + longitude,
                            Toast.LENGTH_LONG).show();

                } else {
                }
                Log.e("reeach","NAKA 2");
                mDatabase = FirebaseDatabase.getInstance().getReference();
                User user = new User(location);
                long t =  (new Date()).getTime();
                Random random = new Random();
                mDatabase.child("users").child(String.valueOf(99)).setValue(user);
                handler.postDelayed(runnable,60000);
            }
        };


        handler.postDelayed(runnable,3000);

        return START_REDELIVER_INTENT;
    }
}
